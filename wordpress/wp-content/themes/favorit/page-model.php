<?php /* Template name: Модель */ ?>

<?php get_header(); ?>

<article class="article">

  <section class="breadcrumbs">
    <div class="container">
      <?php if(function_exists('bcn_display')) { bcn_display(); }?>
    </div>
  </section>

  <?php $model_substrate = get_field('add_substrate'); ?>
  <section  <?php if (!empty($model_substrate)) { ?> class="substrate section" style="background-image:url('<?=$model_substrate?>')"<?}?> class="section">
      <div class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post();?>
          <h2 class="title"><?php the_title(); ?></h2>
          <div class="content text">
              <?php the_content(); ?>
          </div>
        <?php endwhile; endif; ?>
      </div>
  </section>

  <?php get_template_part( 'parts/content', get_post_format()); ?>

</article>

<?php get_footer(); ?>
