<?php /* Template name: Контакты */ ?>

<?php get_header(); ?>
<section class="breadcrumbs">
  <div class="container">
    <?php if(function_exists('bcn_display')) { bcn_display(); }?>
  </div>
</section>
<section class="contact">
  <div class="container">

    <h2 class="title"><?php the_title(); ?></h2>
    <ul class="contact__list">
      <?php if (have_rows('block')) : while (have_rows('block')) : the_row();?>
        <li class="contact__block">
          <?php $img = get_sub_field('image');  if(!empty($img)):?>
            <div class="contact__img" style="background-image:url('<?=$img?>');"> </div>
          <?php endif; ?>

          <?php $title = get_sub_field('title'); if(!empty($title)): ?>
            <h3 class="subtitle"><?=$title?></h3>
          <?php endif; ?>

          <?php if (have_rows('contacts')) : while (have_rows('contacts')) : the_row();?>

          <?php if (get_row_layout() == 'get_address') : ?>
            <address class="contact__item">
              <i class="contact__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="26.769" viewBox="0 0 20 26.769"><path d="M10.035 26.77c-.406-.426-.748-.77-1.075-1.13-2.795-3.07-5.394-6.287-7.318-9.995C.915 14.245.282 12.8.09 11.222c-.442-3.594.763-6.6 3.52-8.898 5.91-4.93 14.9-1.587 16.256 6.008.373 2.088-.062 4.057-.894 5.968-1.326 3.04-3.22 5.72-5.304 8.268-1.16 1.417-2.395 2.773-3.633 4.2zm6.653-16.705c.006-3.742-2.98-6.757-6.692-6.76-3.682-.004-6.702 3.012-6.708 6.698-.006 3.72 2.968 6.736 6.646 6.745 3.76.01 6.748-2.95 6.754-6.683z"/></svg>
              </i><?php echo get_sub_field('address'); ?>
            </address>

          <?php elseif(get_row_layout() == 'get_mode') : ?>
            <div class="contact__item">
                <i class="contact__icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"  viewBox="0 0 20 20"><path d="M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10S15.5 0 10 0zm5.3 11.3h-5.2c-.6 0-1-.4-1-1v-6c0-.4.1-.7.5-.8.5-.2 1 0 1.2.5v5.6H15.2c.5 0 .8.2.9.6 0 .6-.3 1.1-.8 1.1z"/></svg>
                </i> <?php echo get_sub_field('mode') ?>
            </div>

          <?php elseif(get_row_layout() == 'get_tel') : ?>
              <a href="tel:<?php echo get_sub_field('tel'); ?>" class="contact__item">
                <i class="contact__icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21.46" viewBox="0 0 20 21.46"><path d="M0 6.507c-.014-1.1.235-2.142.71-3.122.265-.54.585-1.065 1.037-1.478C2.34 1.362 2.927.8 3.567.312c.654-.498 1.312-.37 1.92.175.675.603 1.125 1.376 1.583 2.138.266.443.514.904.707 1.382.425 1.047.25 1.977-.715 2.72-.44.34-.887.675-1.296 1.05-.422.386-.594.872-.497 1.466.157.967.66 1.757 1.23 2.508.47.622.99 1.207 1.494 1.802.696.82 1.49 1.538 2.443 2.042.64.338 1.317.455 1.965-.025.078-.06.147-.13.22-.195.502-.447.98-.924 1.51-1.333.636-.488 1.356-.462 2.066-.16.692.296 1.27.765 1.827 1.262.36.32.746.617 1.08.964.37.39.733.79.858 1.352.104.475-.014.895-.34 1.204-.62.593-1.253 1.184-1.945 1.687-.802.584-1.726.935-2.727 1.03-.367.037-.737.09-1.105.08-.97-.022-1.908-.23-2.816-.577-.933-.354-1.8-.83-2.603-1.412-.522-.378-1.032-.778-1.52-1.2-.495-.426-.983-.865-1.427-1.344-.664-.717-1.306-1.456-1.92-2.216-.48-.592-.92-1.216-1.346-1.848C1.416 11.68.784 10.41.39 9.037.23 8.48.16 7.9.065 7.327.022 7.056.02 6.78 0 6.506z"/></svg>
                </i> <?php echo get_sub_field('tel'); ?>
              </a>

            <?php elseif(get_row_layout() == 'get_mail') : ?>
              <a href="mailto:<?php echo get_sub_field('mail'); ?>" class="contact__item">
                <i class="contact__icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="12.3" viewBox="0 0 20 12.3"><path d="M20 1.2v11.2H0V1.2c.8.4 1.6.8 2.4 1.3 1.5.9 2.9 2 4.4 3C5.3 7 4 8.5 2.7 10l.2.2c1.6-1.3 3.2-2.7 4.9-4.1 1.5 1.4 3 1.4 4.5 0 1.7 1.4 3.3 2.8 4.9 4.1l.2-.2c-1.3-1.5-2.6-3-4-4.5 2.1-1.4 4.2-2.8 6.2-4.2.1-.1.2-.1.4-.1z"/><path d="M10 0h8.9c.3 0 .6.2.8.3-.2.2-.3.6-.6.7-2.7 1.5-5.4 3-8.1 4.4-.6.3-1.6.3-2.2 0C6.2 4 3.6 2.5.9 1 .7.8.5.5.3.2.6.2.9 0 1.2 0H10z"/></svg>
                </i>
                <?php echo get_sub_field('mail'); ?>
              </a>

          </li>

      <?php  endif; endwhile; endif; endwhile; endif;?>

    </ul>
  </div>
</section>

<section>
  <?php echo get_field('mapfield'); ?>
</section>


<?php get_footer(); ?>
