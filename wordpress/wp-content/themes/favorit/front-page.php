<?php get_header(); ?>


<?php $banner_ch = get_field('banner-ch', 'option'); if($banner_ch) :?>
<section class="front-slider">
  <?php $height = get_field('height', 'option');
        $awidth = get_field('awidth', 'option');
        $aheight = get_field('aheight', 'option')
  ?>

  <?php $slides = get_field('front-slider', 'option'); if(!empty($slides)) : ?>
  <ul class="front-slider__list">
    <?php foreach ($slides as $slide) : ?>
      <li class="front-slider__item"
        style="background-image:url('<?php echo $slide['url'] ?>');
        <?php if(!empty($height)) {echo 'height:'; echo $height; echo 'px;'; } ?>"></li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>

  <div class="container" <?php if(!empty($height)) {echo 'style="height:'; echo $height; echo 'px;"'; } ?>>
    <?php $front_offers = get_field('front-offer','option'); if(!empty($front_offers)) : ?>
      <div class="offer-slider__wrap" <?php if(!empty($awidth)) {echo 'style=width:'; echo $awidth; echo 'px;"'; } ?> >
        <div class="slider-controls">
          <a href="javascript:;" class="slider-prev">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.4 20"  fill="#fff"><path d="M7.8 20h5.6L5.6 10l7.8-10H7.8L0 10"/></svg>
          </a>
          <a href="javascript:;" class="slider-next">
            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 13.4 20" fill="#fff"><path d="M5.6 0H0l7.8 10L0 20h5.6l7.8-10"/></svg>

          </a>
        </div>

        <ul class="offer-slider__list">
          <?php foreach ($front_offers as $front_offer): ?>
            <li class="offer-slider__item" <?php if(!empty($aheight)) {echo 'style="height:'; echo $aheight; echo 'px;"'; } ?>>
              <h2 class="offer-slider__title">
                <?php echo $front_offer['title']; ?>
              </h2>
              <div class="offer-slider__subtitle">
                <?php echo $front_offer['subtitle']; ?>
              </div>
              <a href="<?php echo $front_offer['url']; ?>" class="offer-slider__more"> подробнее</a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <?php endif; ?>
  </div>
</section>
<?php endif; ?>


<?php $service_ch = get_field('service-ch', 'option'); if($service_ch) : ?>
<section class="front-service">
  <div class="container">
    <div class="title">Услуги
      <p class="subtitle">по ремонту и ТО Mercedes-Benz</p>
    </div>
    <?php
      $main_serv = new WP_Query(array(
        'post_type' => 'services',
        'post_status' => 'publish',
        'posts_per_page' => 6,
        'tax_query' => array(
      		array(
      			'taxonomy' => 'services_category',
      			'field'    => 'slug',
      			'terms'    => 'main',
      		),
      	)

      ));
    ?>
    <ul class="services__list">
      <?php if ($main_serv -> have_posts()) : ?>
        <?php while  ( $main_serv -> have_posts()) : $main_serv -> the_post();  ?>
        <li class="services__item">
          <a href="<?php the_permalink(); ?>" class="services__link">
              <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small '); if(!empty($img)) : ?>
              <div class="services__img" style="background-image: url('<?php echo $img[0] ?>');"> </div>
            <div class=""> <?php the_title(); ?> </div>
            <div class="services__btn"> </div>
          </a>
        </li>
        <?php wp_reset_postdata(); wp_reset_query(); ?>
      <?php endif; endwhile; endif; ?>
    </ul>
  </div>
</section>
<?php endif; ?>

<?php $other_ch = get_field('other-ch', 'option'); if($other_ch) : ?>
<section class="other-service">
  <div class="container">
    <?php
      $other_serv = new WP_Query(array(
        'post_type' => 'services',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        'tax_query' => array(
      		array(
      			'taxonomy' => 'services_category',
      			'field'    => 'slug',
      			'terms'    => 'extra',
      		),
      	)
      ));
    ?>
    <ul class="other-service__list">
      <?php if ($other_serv -> have_posts()) : ?>
        <?php while  ( $other_serv -> have_posts()) : $other_serv -> the_post();  ?>
        <li class="other-service__item">
          <a href="<?php the_permalink(); ?>" class="other-service__link">
            <div class="other-service__img">
              <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small '); if(!empty($img)) : ?>
              <img src="<?php echo $img[0] ?>" alt="" />
            </div>
            <div class="other-service__title"> <?php the_title(); ?> </div>
          </a>
        </li>
        <?php wp_reset_postdata(); wp_reset_query(); ?>
      <?php endif; endwhile; endif; ?>
    </ul>
  </div>
</section>
<?php endif; ?>

<?php $model_ch = get_field('model-ch', 'option'); if($model_ch) : ?>
<section class="models">
  <div class="container">
    <div class="title">Выберите серию и модель
      <p class="subtitle">для уточнения стоимости ремонта</p>
    </div>
    <div class="models-nav">
      <?php
        $models = array(
          'walker' => new Car_Menu_Walker($hex_width),
        );
        wp_nav_menu( $models );
      ?>
    </div>
  </div>
</section>
<?php endif; ?>

<?php $offers_ch = get_field('offers-ch', 'option'); if($offers_ch) : ?>
<section class="front-offers dark">
  <div class="container">
    <div class="front-offers__block">
      <div class="title">Акции</div>
      <ul class="offers__list">
        <?php
          $offers = new WP_Query(array(
            'post_type' => 'offers',
            'post_status' => 'publish',
            'posts_per_page' => 3,
          ));
        ?>
        <?php if ($offers -> have_posts()) : ?>
          <?php while  ( $offers -> have_posts()) : $offers -> the_post();  ?>
            <li class="offers__item">
              <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small '); ?>
              <div class="offers__img" <?php if(!empty($img)) : ?> style="background-image: url('<?php echo $img[0] ?>');" <?php endif; ?> >
                <?php $offer_date = get_field('time');
                if(strlen($offer_date)>0){ ?>
                <div class="offers__date">
                  Акция до <?php echo $offer_date; ?>
                  <?php /**/ ?>
                </div>
                <?php } ?>
               </div>
              <div class="offers__wrap">
                <a href="<?php the_permalink(); ?>" class="offers__title"> <?php the_title(); ?> </a>
                <div class="text">
                  <?php echo get_the_excerpt(); ?>
                  <a href="<?php the_permalink(); ?>" class="offers__more">Подробнее </a>
                </div>
              </div>
            </li>
            <?php wp_reset_postdata(); wp_reset_query(); ?>
          <?php endwhile; endif;  ?>
      </ul>
    </div>

    <div class="front-offers__block">
      <div class="title">Советы </div>
      <ul class="offers__list">

        <?php
          $advance = new WP_Query(array(
            'post_type' => 'post',
            'post_satus' => 'publish',
            'posts_per_page' => 3,
            'cat' => 5
          ));
         ?>

         <?php if ($advance->have_posts()) : ?>
           <?php while  ( $advance->have_posts()) : $advance->the_post(); ?>
             <li class="offers__item">
               <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small '); ?>
               <div class="offers__img" <?php if(!empty($img)) : ?> style="background-image: url('<?php echo $img[0] ?>');" <?php endif; ?> > </div>
              <div class="offers__wrap">
                 <a href="<?php the_permalink(); ?>" class="offers__title"> <?php the_title(); ?> </a>
                 <div class="text">
                   <?php echo get_the_excerpt(); ?>
                   <a href="<?php the_permalink(); ?>" class="offers__more">Подробнее </a>
                 </div>
              </div>
             </li>
             <?php wp_reset_postdata(); wp_reset_query(); ?>
           <?php endwhile; endif;  ?>
      </ul>
    </div>

  </div>
</section>
<?php endif; ?>

<?php $news_ch = get_field('news-ch', 'option'); if($news_ch) : ?>
<?php $news_bg = get_field('news-bg', 'option'); ?>
<section class="front-news other-posts" <?php if(!empty($news_bg)) : ?>style="background-image:url('<?=$news_bg?>');" <?php endif;?>>
  <div class="container">
    <h2 class="title"> Новости</h2>
    <ul class="post__list">
      <?php
          $news = new WP_Query(array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 3,
            'cat' => 1
          ));
        ?>

      <?php if ($news -> have_posts()) : ?>
        <?php while  ( $news -> have_posts()) : $news -> the_post();  ?>
          <?php $cat = get_the_category(); ?>
          <li class="post__item">
            <div class="post__inner">
              <a href="<?php the_permalink();?>" class="post__title"> <?php the_title(); ?> </a>
              <div class="post__date"> <?php the_date('d.m.y') ?> </div>
              <div class="text"><?php the_excerpt(); ?></div>
              <a  class="post__more" href="<?php the_permalink();?>" > </a>
            </div>
          </li>
          <?php wp_reset_postdata(); wp_reset_query(); ?>
        <?php endwhile;  endif;?>
    </ul>
  </div>
</section>
<?php endif; ?>

<?php $about_ch = get_field('about-ch', 'option'); if($about_ch) : ?>
  <section class="front-offers">
    <div class="container">
      <div class="front-offers__block">
        <div class="title"><?php echo get_field('about_title', 'option'); ?> </div>
        <div class="text">
          <?php echo get_field('about_text', 'option'); ?>
        </div>
        <?php $about_link =  get_field('about_url', 'option');?>
        <?php if (!empty($about_link)): ?>
          <a href="<?=$about_link?>" class="offers__more">Подробнее </a>
        <?php endif; ?>
      </div>
      <div class="front-offers__block" style="background:url('<?php echo get_field('about_img', 'option') ?>') left center no-repeat / cover;">
      </div>
    </div>
  </section>
<?php endif; ?>


<?php get_footer(); ?>
