<?php get_header(); ?>

<article class="article">
  <div class="container">
    <ul class="post__list">
      <?php if (have_posts()) : while (have_posts()) : the_post();?>

          <li class="post__item">
            <a href="<?php the_permalink();?>">
              <h3 class="post__title"> <?php the_title(); ?> </h3>
              <div class="post__date"> <?php the_date('d.m.y') ?> </div>
            </a>
          </li>

      	<?php endwhile;  endif;?>
    </ul>

  </div>
</article>

<?php get_footer(); ?>
