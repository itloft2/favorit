<?php get_header(); ?>


<article class="dark">

  <section class="breadcrumbs">
    <div class="container">
      <?php if(function_exists('bcn_display')) { bcn_display(); }?>
    </div>
  </section>

  <section class="news-archive">
    <div class="container">
      <div class="title"> новости и рекомендации </div>
      <?php
        $cat = array(
          'show_option_all'    => 'все',
          'orderby'            => 'id',
          'title_li'           => '',
        );
      ?>
      <ul class="cat-list">
        <?php echo wp_list_categories($cat); ?>
      </ul>

      <ul class="post__list">
        <?php if (have_posts()) : while (have_posts()) : the_post();?>
            <?php $cat = get_the_category(); ?>
            <li class="post__item">
              <div class="post__wrap">
                <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small '); if(!empty($img)) : ?>
                  <div class="post__img" style="background-image: url('<?php echo $img[0] ?>');"> </div>
                <?php endif; ?>
                <div class="post__inner">
                  <div class="post__tag">
                    <ul class="post-categories">
                      <?php foreach($cat as $category) { ?>
                        <li class='<?php echo $category->slug; ?>'>
                          <a href='<?php echo get_category_link($category->cat_ID); ?>'><?php echo $category->name; ?></a>
                        </li>
                    	<?php } ?>
                    </ul>
                  </div>
                  <a href="<?php the_permalink();?>" class="post__title"> <?php the_title(); ?> </a>
                  <div class="post__date"> <?php the_date('d.m.y') ?> </div>
                  <div class="text"><?php the_excerpt(); ?></div>
                  <a class="post__more" href="<?php the_permalink();?>" > </a>
                </div>
              </div>
            </li>
        <?php endwhile;  endif;?>
      </ul>

      <div class="pagination">
        <?php $args = array(
            'prev_text'    => __('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.4 20" ><path d="M7.8 20h5.6L5.6 10l7.8-10H7.8L0 10"/></svg>'),
          	'next_text'    => __('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.4 20" ><path d="M5.6 0H0l7.8 10L0 20h5.6l7.8-10"/></svg>'),
        ); ?>
        <?php echo paginate_links( $args ) ?>
      </div>
    </div>
  </section>
  
</article>

<?php get_footer(); ?>
