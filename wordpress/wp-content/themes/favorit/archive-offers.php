<?php get_header(); ?>


<article class="dark">
  <div class="container">
    <section class="breadcrumbs">
        <?php if(function_exists('bcn_display')) { bcn_display(); }?>
    </section>

    <section class="offers">
      <div class="title"> <?php post_type_archive_title(); ?> </div>
      <ul class="offers__list">
        <?php query_posts("post_type=offers&posts_per_page=-1"); ?>
        <?php if (have_posts()) :  while (have_posts()) : the_post();?>
          <li class="offers__item">
              <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small '); ?>
              <a href="<?php the_permalink(); ?>" class="offers__img" <?php if(!empty($img)) : ?> style="background-image: url('<?php echo $img[0] ?>');" <?php endif; ?> >
                <?php $offer_date = get_field('time');
                if(strlen($offer_date)>0){ ?>
                <div class="offers__date">
                  Акция до <?php echo $offer_date; ?>
                  <?php /**/ ?>
                </div>
                <?php } ?>
              </a>
              <a href="<?php the_permalink(); ?>" class="offers__title"> <?php the_title(); ?> </a>
              <div class="text">
                <?php echo get_the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>" class="offers__more">Подробнее </a>
              </div>

          </li>
        <?php endwhile; endif; ?>
      </ul>
    </section>
  </div>
</article>


<?php get_footer(); ?>
