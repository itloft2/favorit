<!DOCTYPE html>
<html lang="ru-RU">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="ru">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="initial-scale=0, maximum-scale=0">
    <title><?php echo bloginfo('name'); ?></title>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <style>
      <?php
        $accent = get_field('accent', 'options');
        $hover = get_field('hover', 'options');
      ?>
      .slider__list .owl-prev, .slider__list .owl-next, .footer__contact-wrap,
      .post__more, .call-btn, .cat-list a:hover,  .slider-prev, .slider-next,
      .services__btn,
      .front-slider__list .owl-dots .owl-dot.active span,
      .btn { background-color: <?=$accent?>; }

      /*hover*/
      .slider__list .owl-prev:hover, .slider__list .owl-next:hover,
      .post__more:hover, .call-btn:hover, .slider-prev:hover, .slider-next:hover,
      .btn:hover, .services__btn:hover { background-color: <?=$hover?>; }

      /*color*/
      .breadcrumbs a, .menu_title:hover, .models-nav .sub-menu .menu_title:hover,
      .offers__title:hover, .class__list a:hover, .menu__list a:hover, .main-nav a:hover,
      .logo__block span, .offers__more, .services__link:hover, .other-service__link:hover { color: <?=$accent?>; }

      .title { border-left-color: <?=$accent?>; }
      .contact__icon svg { fill: <?=$accent?>; }
      .menu_models:before { border-color: <?=$accent?> transparent transparent transparent; }

      /*alpha*/
      .offer-slider__list,
      .offers__date {
        background-color: <?=hex2rgba($accent)?>;
      }
    </style>
    <?php wp_head(); ?>
    <?php 
    $some_html = get_field('html_in_head');
    if(strlen($some_html)>0){
      echo $some_html;
    };
    ?>

  </head>


  <body>
  <?php 
    $some_html = get_field('html_in_header');
    if(strlen($some_html)>0){
      echo $some_html;
    };
    ?>
    <div class="wrapper">
      <div class="modal">
        <div class="modal__content">
          <a href="javascript:;" class="modal__close"></a>
          <div class="modal__default">
            <h2 class="modal__title">Заказ запчастей</h2>
            <div class="text">
              Оставьте свои контакты, и мы свяжемся с вами в ближайшее время
            </div>
            <form id="callback-form"  method="post">
              <input type="hidden" name="title" value="" />
              <label for="name" class="label" >Ваше имя</label>
              <input type="text" class="input" id="name" name="name" value="" placeholer="Александр" >
              <label for="tel" class="label">Ваш телефон</label>
              <input type="text" class="input" id="tel" name="tel" value="" placeholer="+7 (___) ___-__-__">
              <label for="txt" class="label">Комментарий</label>
              <textarea name="txt" rows="8" cols="40" class="textarea" id="txt"></textarea>
              <button type="submit" name="button" class="btn">Свяжитесь со мной</button>
            </form>
          </div>
          <div class="modal__success"> Ваша заявка принята </div>
        </div>
      </div>
      <div class="overlay"></div>

      <header class="header">
        <div class="container">
          <a href="/" class="logo__wrap">
            <div class="logo__icon">
              <img src="<?php echo get_field('logo', 'option'); ?>" alt="" />
            </div>
            <div class="logo__block">
              <?php echo get_field('name', 'option'); ?>
              <span><?php echo get_field('company', 'option'); ?></span>
            </div>
          </a>
          <div class="header__nav">
            <?php
              $nav = array(
                'theme_location'  => 'main_menu',
                'container'       => 'nav',
                'container_class' => 'main-nav',
                'items_wrap'      => '<ul>%3$s</ul>',
              );

              wp_nav_menu( $nav );
            ?>
            <div class="extra-menu">
              <?php
                $nav = array(
                  'theme_location'  => 'extra_menu',
                  'container'       => 'ul',
                  'items_wrap'      => '<ul class="menu__list">%3$s</ul>',
                );

                wp_nav_menu( $nav );
              ?>
              <?php if(!get_field('header_hide_1st_btn')): ?>
                <?php $bgicon = get_field('header_1st_btn_icon','option'); ?>
              <a href="javascript:;"
                <?php $event = get_field('header_1st_event','option');
                if(strlen($event)>0): ?>
                  onclick="<?=$event;?>"
                <?php endif; ?>
                class="order-btn" <?php if(strlen($bgicon)) { ?>style="background-image:<?=$bgicon;?>"<?php }; ?>><?php the_field('header_1st_btn_title','option');?></a>
              <?php endif; ?>
            </div>
          </div>

          <div class="header__contact">
            <?php $htel = get_field('htel', 'option'); if($htel) :?>
              <a href="tel:<?=$htel?>" class="header__tel"><?=$htel?></a>
            <?php endif; ?>
            
            <?php if(!get_field('header_hide_2st_btn')): ?>
                <?php $bgicon = get_field('header_2st_btn_icon','option'); ?>
              <a href="javascript:;" 
              <?php $event = get_field('header_2st_event','option');
                if(strlen($event)>0): ?>
                  onclick="<?=$event;?>"
                <?php endif; ?>
              class="call-btn" <?php if(strlen($bgicon)){ ?>style="background-image:<?=$bgicon;?>"<?php } ?>><?php the_field('header_2st_btn_title','option');?></a>
            <?php endif; ?>
          </div>

        </div>
        <?php $class = get_field('model-check', 'option'); if($class) : ?>
          <?php $bg_class = get_field('bg_class', 'option'); ?>
          <div <?php if (!empty($bg_class)) { ?> style="background-color: <?=$bg_class?>;" <?}?> class="class">
            <div class="container">
              <h4 class="class__title"><?php echo get_field('model-title', 'option');  ?></h4>
                <?php
                  $nav = array(
                    'theme_location'  => 'class_menu',
                    'container'       => 'ul',
                    'items_wrap'      => '<ul class="class__list">%3$s</ul>',
                  );

                  wp_nav_menu( $nav );
                ?>
              <a href="javascript:;" class="to-Top">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" viewBox="0 0 20 13.4" fill="#383838"><path d="M10 0l10 7.8v5.6L10 5.6 0 13.4V7.8L10 0z"/></svg>
              </a>
            </div>
          </div>
        <?php endif; ?>
      </header>
      <main>
