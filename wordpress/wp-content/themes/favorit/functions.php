<?php

require 'vendor/autoload.php';

if(function_exists('acf_add_options_page')){
  acf_add_options_page();
  acf_add_options_page('Главная страница');
}


  add_theme_support( 'post-thumbnails' );

  register_nav_menus( array(
      'main_menu' => __( 'Main Menu'),
      'extra_menu' => __( 'Extra Menu'),
      'class_menu' => __( 'Models Menu'),
      'models_menu' => __( 'Models Menu'),
      'footer-list-1' => __( 'List 1'),
      'footer-list-2' => __( 'List 2'),
      'footer-list-3' => __( 'List 3'),
  ) );

  function thumb_or($post = null) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
        $img = wp_get_attachment_image_src($post_thumbnail_id, 'large');
      if(!empty($img))
          return $img[0];
      else
          return "/img/bg/cover.png";
  }

//  get_the_excerpt length
  function new_excerpt_length($length) {
  	return 15;
  }
  add_filter('excerpt_length', 'new_excerpt_length');




  // Register Custom Post Type
  function services() {

  	$labels = array(
  		'name'                       => _x( 'Категории Услуг', 'Taxonomy General Name', 'text_domain' ),
  		'singular_name'              => _x( 'Категория Услуг', 'Taxonomy Singular Name', 'text_domain' ),
  		'menu_name'                  => __( 'Категория услуг', 'text_domain' ),
  		'all_items'                  => __( 'Все категории', 'text_domain' ),
  		'parent_item'                => __( 'Родительская категория', 'text_domain' ),
  		'parent_item_colon'          => __( 'Родитель:', 'text_domain' ),
  		'new_item_name'              => __( 'Новая категория', 'text_domain' ),
  		'add_new_item'               => __( 'Добавить категорию', 'text_domain' ),
  		'edit_item'                  => __( 'Изменить', 'text_domain' ),
  		'update_item'                => __( 'Обновить', 'text_domain' ),
  		'view_item'                  => __( 'Посмотреть', 'text_domain' ),
  		'separate_items_with_commas' => __( 'разделите запятыми', 'text_domain' ),
  		'add_or_remove_items'        => __( 'Добавить/удалить категории', 'text_domain' ),
  		'choose_from_most_used'      => __( 'Выберите из часто используемых', 'text_domain' ),
  		'popular_items'              => __( 'Популярные', 'text_domain' ),
  		'search_items'               => __( 'Найти', 'text_domain' ),
  		'not_found'                  => __( 'Не найдено', 'text_domain' ),
  		'no_terms'                   => __( 'Нет элементов', 'text_domain' ),
  		'items_list'                 => __( 'Список элементов', 'text_domain' ),
  		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  	);
  	$args = array(
  		'labels'                     => $labels,
  		'hierarchical'               => true,
  		'public'                     => true,
  		'show_ui'                    => true,
  		'show_admin_column'          => true,
  		'show_in_nav_menus'          => true,
  		'show_tagcloud'              => true,
  	);
  	register_taxonomy( 'services_category', array( 'services' ), $args );

  	$labels = array(
  		'name'                  =>  'Услуги',
  		'singular_name'         =>  'Услуги',
  		'menu_name'             =>  'Услуги',
  		'name_admin_bar'        =>  'Услуги',
  		'all_items'             =>  'Все услуги',
  		'add_new_item'          =>  'Добавить новую услугу',
  		'add_new'               =>  'Добавить новую',
  		'edit_item'             =>  'Редактировать услугу',
  		'update_item'           =>  'Обновить',
      'view_item'             =>  'Просмотреть',
      'search_items'          =>  'Найти',
  		'not_found'             =>  'Не найден',
      'not_found_in_trash'    =>  'Не найден в корзине',
      'featured_image'        =>  'Изображение',
      'set_featured_image'    =>  'Установить изображение',
      'remove_featured_image' =>  'Удалить изображение',
      'use_featured_image'    =>  'Использовать изображение',
      'insert_into_item'      =>  'Вставить в',
      'uploaded_to_this_item' =>  'Загрузить',
      'items_list'            =>  'Список',
      'items_list_navigation' =>  'Список',
      'filter_items_list'     =>  'Фильтр',
  	);
  	$args = array(
  		'label'                 => 'Услуга',
  		'description'           => 'Описание услуги',
  		'labels'                => $labels,
  		'supports'              => array('title', 'editor', 'thumbnail',),
  		'taxonomies'            => array( 'post_tag', 'services_category' ),
  		'hierarchical'          => false,
  		'public'                => true,
  		'show_ui'               => true,
  		'show_in_menu'          => true,
  		'menu_position'         => 5,
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => true,
  		'exclude_from_search'   => false,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
  	);
  	register_post_type( 'services', $args );

  }
  add_action( 'init', 'services', 0 );

  // Register Custom Post Type
  function offers() {

    $labels = array(
      'name'                  =>  'Акции и спецпредложения',
      'singular_name'         =>  'Акции',
      'menu_name'             =>  'Акции',
      'name_admin_bar'        =>  'Акции',
      'all_items'             =>  'Все акции',
      'add_new_item'          =>  'Добавить новую акцию',
      'add_new'               =>  'Добавить новую',
      'edit_item'             =>  'Редактировать акцию',
      'update_item'           =>  'Обновить',
      'view_item'             =>  'Просмотреть',
      'search_items'          =>  'Найти',
      'not_found'             =>  'Не найден',
      'not_found_in_trash'    =>  'Не найден в корзине',
      'featured_image'        =>  'Изображение',
      'set_featured_image'    =>  'Установить изображение',
      'remove_featured_image' =>  'Удалить изображение',
      'use_featured_image'    =>  'Использовать изображение',
      'insert_into_item'      =>  'Вставить в',
      'uploaded_to_this_item' =>  'Загрузить',
      'items_list'            =>  'Список',
      'items_list_navigation' =>  'Список',
      'filter_items_list'     =>  'Фильтр',
    );
    $args = array(
      'label'                 => 'Акиця',
      'description'           => 'Описание акции',
      'labels'                => $labels,
      'supports'              => array('title', 'editor', 'thumbnail',),
      'taxonomies'            => array( 'category', 'post_tag' ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );
    register_post_type( 'offers', $args );

  }
  add_action( 'init', 'offers', 0 );

  class Car_Menu_Walker extends Walker_Nav_Menu {
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
  		// $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
  		// $classes = empty( $item->classes ) ? array() : (array) $item->classes;
  		// $classes[] = 'menu-item-' . $item->ID;
  		// $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
  		// $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
  		// $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
  		// $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
  		// $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
  		// $output .= $indent . '<li' . $id . $class_names .'>';
      $output .=  '<li>';

  		$atts = array();
  		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
  		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
  		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
  		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

  		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

  		$attributes = '';
  		foreach ( $atts as $attr => $value ) {
  			if ( ! empty( $value ) ) {
  				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
  				$attributes .= ' ' . $attr . '="' . $value . '"';
  			}
  		}

  		/** This filter is documented in wp-includes/post-template.php */
  		$title = apply_filters( 'the_title', $item->title, $item->ID );

  		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );


      $item_output = $args->before;


  		$item_output .= '<a'. $attributes .'>';
      $item_id = (int)$item->object_id;
      if($depth == 0){
        $item_output .= '<div class="menu_thumb" style="background-image:url(' . wp_get_attachment_image_src( get_post_thumbnail_id($item_id), 'medium')['0'] . ')" title="' . get_the_title($item_id) .'"</div></div>';
      }
      $item_output .= $args->link_before . '<div class="menu_title">' . $title . '</div>' . $args->link_after;
  		$item_output .= '</a>';
      if($depth == 0){
        $item_output .= '<div class="menu_models">Модель</div>';
      }
  		$item_output .= $args->after;

  		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  	}
  };

  function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');


//
  function get_callback () {
      header('Content-Type: application/json');
      if (isset($_POST['name']) && isset($_POST['tel']) && isset($_POST['nonce'])) {
          if (!wp_verify_nonce($_POST['nonce'], 'callback')) {
              echo json_encode([
                  'status'  => 'wrong',
                  'message' => 'bad nonce',
              ]);
              die();
          }
          //

          $send = sendForm($_POST['name'], $_POST['tel'], $_POST['title'], $_POST['txt']);
          if ($send == "ok") {
            echo json_encode([
              'status' => 'ok',
            ]);
          }
          else {
            echo json_encode([
              'status'  => 'wrong',
              'message' => $send,
            ]);
          }

          die();
      }
      else {
          echo json_encode([
              'status'  => 'wrong',
              'message' => 'not enough params',
          ]);
          die();
      }
  }


  function hex2rgba($hex, $alpha = 0.8)
  {
    $hex = str_replace("#", "", $hex);
    $r = hexdec(substr($hex,0,2));
    $g = hexdec(substr($hex,2,2));
    $b = hexdec(substr($hex,4,2));
    return 'rgba(' . $r . ',' . $g . ',' . $b . ',' . $alpha .')';
  }

  function sendForm ($name, $tel, $title, $txt) {

    $email_to = get_option("admin_email");
    $email_subject = "Письмо с сайта " . get_field('site_url', 'option');

    $mail = new PHPMailer;

    $mail->IsSMTP();
    $mail->SMTPAuth   = true;
    $mail->Host       = "smtp.yandex.ru";
    $mail->Username   = "no-reply@itloft.ru";
    $mail->Password   = 'y9pfoKB9OPrCQaqo39';
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, ssl also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom('no-reply@itloft.ru', 'E-mail с сайта');
    $mail->addAddress($email_to, 'info');     // Add a recipient
    $mail->addReplyTo("no-reply@itloft.ru", 'No Reply');
    $mail->CharSet = 'UTF-8';
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $email_subject.date('d.m.Y H:i:s',time());

    function clean_string($string)
    {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }

    $email_message .= "Форма: ".clean_string($title)."<br>";

    $email_message .= "Имя: ".clean_string($name)."<br>";
    if(isset($txt) && strlen($txt)>0){
      $email_message .= "Комментарий: ".clean_string($txt)."<br>";
    }
    if(isset($tel) && strlen($tel)>0){
        $email_message .= "Телефон: ".clean_string($tel)."<br>";

        $mail->Body    = $email_message;
        $mail->AltBody = $email_message;

        if($mail->send()){
          return "ok";
        }else{
          return $mail->ErrorInfo;
        }
    }


  }

  if (defined('DOING_AJAX') && DOING_AJAX) {
      add_action('wp_ajax_get_callback', 'get_callback');
      add_action('wp_ajax_nopriv_get_callback', 'get_callback');
  }

?>
