<?php get_header(); ?>

<section class="breadcrumbs">
  <div class="container">
    <?php if(function_exists('bcn_display')) { bcn_display(); }?>
  </div>
</section>

<article class="article">
  <?php $post_substrate = get_field('add_substrate'); ?>
  <section <?php if (!empty($post_substrate)) { ?> class="substrate" style="background-image:url('<?=$post_substrate?>')"<?}?>>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="container">
        <div class="content">
          <h2 class="article__title"><?php the_title(); ?></h2>
          <div class="article__date"><?php the_date('d.m.y') ?> </div>
          <div class="text"><?php the_content(); ?> </div>
        </div>
      </div>
  	<?php endwhile; endif; ?>
  </section>

  <?php get_template_part( 'parts/content', get_post_format()); ?>
</article>

<section class="section other-posts dark">
  <div class="container">
    <h2 class="title"> Другие новости</h2>

    <ul class="post__list">
      <?php
          $news = new WP_Query(array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 3,
          ));
        ?>

      <?php if ($news -> have_posts()) : ?>
        <?php while  ( $news -> have_posts()) : $news -> the_post();  ?>
          <?php $cat = get_the_category(); ?>

          <li class="post__item">
            <div class="post__inner">
              <a href="<?php the_permalink();?>" class="post__title"> <?php the_title(); ?> </a>
              <div class="post__date"> <?php the_date('d.m.y') ?> </div>
              <div class="text"><?php the_excerpt(); ?></div>
              <a  class="post__more" href="<?php the_permalink();?>" > </a>
            </div>
          </li>

        <?php endwhile;  endif;?>
    </ul>
  </div>
</section>




<?php get_footer(); ?>
