<?php if (have_rows('article')) : ?>
  <?php while (have_rows('article')) : the_row(); ?>

    <?php if (get_row_layout() == 'get_slider') : ?>
    <?php $slider_h = get_sub_field('slider_h'); ?>
    <?php $slider = get_sub_field('slider'); if (!empty($slider)) : ?>
      <section class="section">
      <ul class="slider__list lightgallery">
         <?php foreach ($slider as $slide ) : ?>
           <li class="slider__item" style="background-image:url('<?php echo $slide['url']?>'); <?php if(!empty($slider_h)) {echo 'height:'; echo $slider_h; echo 'px;'; } ?>">
             <a href="<?php echo $slide['url']?>" class="lg__photo"> </a>
           </li>
         <?php endforeach; ?>
      </ul>
    </section>
    <?php endif; ?>

    <?php elseif(get_row_layout() == 'get_photogallery') : ?>
    <?php $photogallery = get_sub_field('photogallery'); if (!empty($photogallery)) : ?>
      <section class="section">
        <div class="content">
          <div class="photogallery" >
            <?php foreach ($photogallery as $photo) : ?>
              <a href="<?php echo $photo['url'] ?>"><img src="<?php echo $photo['url'] ?>"></a>
            <?php endforeach; ?>
          </div>
          <h4><?php echo get_sub_field('caption'); ?></h4>
        </div>
      </section>
    <?php endif; ?>

    <?php elseif (get_row_layout() == 'get_textarea') : ?>
    <?php $text = get_sub_field('text');
          $text_substrate = get_sub_field('substrate'); ?>
    <?php if (!empty($text)) : ?>
      <section <?php if(!empty($text_substrate)) : ?> class="substrate" style="background-image: url('<?=$text_substrate?>');"  <?php endif; ?> >
        <div class="content">
          <div class="text"> <?php echo $text ?> </div>
        </div>
      </section>
    <?php endif; ?>

    <?php elseif (get_row_layout() == 'get_table') : ?>
      <?php $table = get_sub_field('table');
            $extra = get_sub_field('extra');
            $substrate = get_sub_field('substrate');
            $color = get_sub_field('color'); ?>

      <?php if ( $table ) : ?>
        <section <?php if(!empty($substrate)) : ?> class="substrate section" style="background-image: url('<?=$substrate?>');"  <?php endif; ?> class="section">

        <div class="content">
          <?php $caption = get_sub_field('caption'); if(!empty($caption)) :?>
          <div class="table__caption"> <?=$caption?> </div>
          <?php endif; ?>
           <table border="0" class="table">
            <?php if( $table['header']) :  ?>
              <thead>
                <tr>
                  <?php foreach ($table['header'] as $th) : ?>
                    <th> <?php echo $th ['c']; ?> </th>
                  <?php endforeach; ?>
                </tr>
              </thead>
            <?php endif; ?>
            <tbody>
              <?php foreach ($table['body'] as $tr) : ?>
                <tr>
                  <?php foreach ($tr as $td) : ?>
                  <?php if(!empty($td['c'])): ?>  <td <?php if(!empty($color)){ ?>style="border-color:<?=$color?>"<?}?>> <?php   echo $td['c']; ?> </td> <?php endif; ?>
                  <?php endforeach; ?>
                </tr>
              <?php endforeach; ?>

              <?php if ($extra) : ?>
                <?php  foreach ($extra as $item) :?>
                  <tr>
                    <td class="table__row" colspan="<?php echo $item['extra_col']; ?>" style="<?php if($item['fill']){ ?>background-color:#f2f2f2;"<?} ?>>
                      <?php echo $item['extra_row']; ?>
                    </td>
                  </tr>
                  <?php $row = $item['extra_table'] ?>
                  <?php foreach ($row['body'] as $set) : ?>
                    <tr>
                      <?php foreach ($set as $cell) : ?>
                        <td <?php if(!empty($color)){ ?>style="border-color:<?=$color?>"<?}?>> <?php echo $cell['c']; ?> </td>
                      <?php endforeach; ?>
                    </tr>
              <?php endforeach; endforeach;  endif;?>
            </tbody>
          </table>
        </div>
      </section>

      <?php endif; ?>

    <?php elseif (get_row_layout() == 'get_tile') : ?>
      <?php $tiles = get_sub_field('tiles');  if (!empty($tiles)) :?>
      <section class="tile dark" >
        <div class="container">
          <ul class="services__list">
            <?php foreach ($tiles as $tile) : ?>
              <li class="services__item">
                <a href="<?php echo $tile['url'] ?>" class="services__link">
                  <div class="services__img" style="background-image: url('<?php echo $tile['img'] ?>');"> </div>
                  <div class=""> <?php echo $tile['title']; ?> </div>
                  <div class="services__btn"> </div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </section>
    <?php endif; ?>


  <?php elseif (get_row_layout() == 'get_services') : ?>
    <?php $services = get_sub_field('services');  if (!empty($services)) :?>
      <section class="other-service">
        <div class="container">
          <ul class="other-service__list">
            <?php foreach ($services as $i) : ?>
              <li class="other-service__item">
                <a href="<?php echo $i['url'] ?>" class="other-service__link">
                  <div class="other-service__img">
                    <img src="<?php echo $i['img'] ?>" alt="" />
                  </div>
                  <div class="other-service__title"> <?php echo $i['title']; ?> </div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </section>

  <?php endif; ?>

  <?php elseif (get_row_layout() == 'get_banner') : ?>
    <?php $bg = get_sub_field('bg');
          $banner_h = get_sub_field('banner_h');?>
    <section class="service__banner"  style="background-image: url('<?=$bg?>');<?php if(!empty($banner_h )) {echo 'height:'; echo $banner_h; echo 'px;';}?>">
    </section>

  <?php elseif (get_row_layout() == 'get_video') : ?>
    <?php $v_url = get_sub_field('v_url');?>
    <section class="video-block">
      <div class="content">
        <iframe width="100%" height="440" src="<?=$v_url?>" frameborder="0" allowfullscreen></iframe>
      </div>
    </section>

<?php endif; endwhile; endif; ?>
