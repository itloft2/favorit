<?php get_header(); ?>

<article class="service">
  <?php $bg = get_field('bg');
        $height = get_field('height');?>

  <section class="service__banner" <?php if(!empty($bg)) {?> style="background-image: url('<?=$bg?>');"<?}?>>
    <div class="container" <?php if(!empty($height)) {?> style="min-height:<?=$height?>px;"<?}?>>
      <div class="service__title"><span><?php the_title();?></span></div>
    </div>
  </section>

  <?php $top_substrate = get_field('add_substrate'); ?>
  <section  <?php if (!empty($top_substrate)) { ?> class="substrate service__top" style="background-image:url('<?=$top_substrate?>')"<?}?> class="service__top">
    <div class="container">
      <div class="breadcrumbs">
        <?php if(function_exists('bcn_display')) { bcn_display(); }?>
      </div>
      <?php if (have_posts()) : while (have_posts()) : the_post();?>
        <div class="content text">
          <?php the_content(); ?>
        </div>
      <?php endwhile; endif;?>
    </div>
  </section>

  <section class="section">
    <div class="container">
      <?php $title = get_field('title'); if(!empty($title)) :?>
      <div class="title"><?=$title?>
        <?php $title = get_field('title'); if(!empty($title)) {
          echo '<p class="subtitle">'; echo get_field('subtitle'); echo '</p>';
        } ?>
      </div>
      <?php endif; ?>
    </div>
  </section>

  <?php get_template_part( 'parts/content', get_post_format()); ?>

</article>

<?php get_footer(); ?>
