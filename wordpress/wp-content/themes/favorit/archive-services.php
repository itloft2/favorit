<?php get_header(); ?>

<article class="dark">
  <div class="container">
  <section class="breadcrumbs">
      <?php if(function_exists('bcn_display')) { bcn_display(); }?>
  </section>
  <section class="services">
    <div class="title"> <?php post_type_archive_title(); ?> </div>
    <ul class="services__list">
      <?php query_posts("post_type=services&posts_per_page=-1"); ?>
      <?php if (have_posts()) :  while (have_posts()) : the_post();?>
        <li class="services__item">
          <a href="<?php the_permalink(); ?>" class="services__link">
              <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small '); if(!empty($img)) : ?>
              <div class="services__img" style="background-image: url('<?php echo $img[0] ?>');"> </div>
            <div class=""> <?php the_title(); ?> </div>
            <div class="services__btn"> </div>
          </a>
        </li>
      <?php endif; endwhile; endif; ?>
    </ul>

  </section>
  </div>

</article>


<?php get_footer(); ?>
