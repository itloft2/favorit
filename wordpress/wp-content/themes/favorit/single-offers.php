<?php get_header(); ?>

<article class="article">

  <?php $offers_substrate = get_field('add_substrate'); ?>

  <section <?php if (!empty($offers_substrate)) { ?> class="substrate section" style="background-image:url('<?=$offers_substrate?>')"<?}?> class="section">
    <div class="container">
      <?php if (have_posts()) : while (have_posts()) : the_post();?>
          <div class="title"> <?php the_title();?> </div>
          <div class="text"> <?php the_content(); ?> </div>
    	<?php endwhile; endif;?>
    </div>
  </section>

  <?php get_template_part( 'parts/content', get_post_format()); ?>


</article>

<?php get_footer(); ?>
