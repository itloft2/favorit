
<?php if (comments_open()) { ?>
  <section class="feedback">

    <div class="container">
    <div class="breadcrumbs">
      <?php if(function_exists('bcn_display')) { bcn_display(); }?>
    </div>
    <div class="title"><?php the_title();?></div>

    <div class="content">
        <?php
          $fields = array(
            'author' => '<div class="comments-wrap"><p class="comment-form-author "><label for="author" class="label">' . __( 'Ваше имя*' )  . '</label><input type="text" id="author" name="author" class="input" value="' . esc_attr($commenter['comment_author']) . '" placeholder="Введите имя" pattern="[A-Za-zА-Яа-я]{3,}" maxlength="30" autocomplete="on" tabindex="1" required' . $aria_req . '></p>',

            'email' => '<p class="comment-form-email"><label for="email" class="label">' . __( 'Ваш e-mail* (не будет опубликован)') . '</label><input type="email" id="email" name="email" class="email input" value="' . esc_attr($commenter['comment_author_email']) . '" placeholder="example@example.com" maxlength="30" autocomplete="on" tabindex="2" required' . $aria_req . '></p></div>'
          );

          $args = array(
            'comment_notes_after' => '',
            'comment_notes_before' => '',
            'title_reply' => 'Оставить отзыв',
            'title_reply_after' => '<span id="comments-status">Спасибо за отзыв!</span></h3>',
            'class_submit' => 'btn',
            'comment_field' => '<p class="comment-form-comment"><label for="comment" class="label">' . __( 'Ваш отзыв' ) . '</label><textarea id="comment" name="comment" class="textarea" cols="45" rows="8" aria-required="true" placeholder="Текст отзыва..."></textarea></p>',

            'label_submit' => 'Отправить',
            'fields' => apply_filters('comment_form_default_fields', $fields)

          );
          comment_form($args);
        ?>
    </div>

  </div>
</section>

<section class="section" >

  <div class="content">

    <?php if (get_comments_number() == 0) { ?>
      <div class="">
        Отзывов пока нет
      </div>
    <?php } else { ?>
    <ul class="comment-list">
      <?php
        function verstaka_comment($comment, $args, $depth){
          $GLOBALS['comment'] = $comment; ?>
          <li class="comment-item">
            <div class="comment-meta">
              <?php printf(__('<span class="comment-author" >%s</span>'), get_comment_author_link()) ?>
              <?php printf(__('<span class="comment-date">%s</span> '), get_comment_date()) ?>
            </div>
            <div class="comment-text"> <?php comment_text() ?> </div>
      <?php }
        $args = array(
          'reply_text' => 'Ответить',
          // 'per_page' => 2,
          'callback' => 'verstaka_comment'
        );
        wp_list_comments($args);
      ?>

    </ul>

  <?php } ?>


  <?php } else { ?>
    <h3>Страница с отзывами закрыта</h3>
  <?php }
?>
</div>
</section>
