var modal = (function() {

  var modal = $('.modal'),
      overlay = $('.overlay'),
      def = modal.find('.modal__default'),
      suc = modal.find('.modal__success');

  function _show(e) {
    e.preventDefault();
    var title = $(this).data('title');
    if(title==undefined){
      var text = $(this).text();
      modal.find('.modal__title').html(text);
    }else{
      modal.find('.modal__title').html(title);
    }
    if(!modal.hasClass('modal-show')) {
      modal.addClass('modal-show');
      modal.find('input[name="title"]').val(text);
    }
  }

  function _close(e) {
    e.preventDefault();
    if(modal.hasClass('modal-show')) {
      modal.removeClass('modal-show');
      restart();
    }
  }

  function restart() {
    def.css("display", "block");
    suc.css("display", "none");
  }

  function success() {
    def.css("display", "none");
    suc.css("display", "block");
  }

  function init() {
    $('.show-modal').on('click', _show);
    $('.call-btn').on('click', _show);
    $('.order-btn').on('click', _show);
    $('.modal__close').on('click', _close);
    $('.overlay').on('click', _close);
  }

  return {
    init: init,
    close: _close,
    success: success
  }

}());


var headerScroll = (function() {

  function top() {
    var headerH = $(".header").outerHeight(),
        wrap = $(".wrapper");

    wrap.css("margin-top", headerH);

    $(window).on("scroll", function() {
        var top = $(this).scrollTop();
      if (top > headerH) {
        wrap.addClass("fix-header");
      } else {
        wrap.removeClass("fix-header");
      }
    });
  }

  function btnUp() {
    var root = $('html, body');
    $('.to-Top').on('click', function(){
      root.stop(true).animate({scrollTop:0}, '800', 'swing');
    });
  }

  function init() {
    top();
    btnUp();
  }

  return { init: init }

}());


var banner = (function() {

  return {
    init : function() {

      var front = $('.front-slider__list');
      var offer = $('.offer-slider__list');

      front.owlCarousel({
        onInitialize: function () {
          if ($('.front-slider__item').length === 1) {
            this.settings.loop = false;
            this.settings.nav = false;
            this.settings.dots = false;
          }
        },
        items:1,
        nav: true,
        loop: true,
        animateOut: 'fadeOut'
      });

    // Go to the next/prev item
      $('.slider-next').on('click', function() {
          front.trigger('next.owl.carousel');
          offer.trigger('next.owl.carousel');
      });

      $('.slider-prev').on('click', function() {
          front.trigger('prev.owl.carousel');
          offer.trigger('prev.owl.carousel');
      });

      offer.owlCarousel({
        onInitialize: function () {
          if ($('.offer-slider__item').length === 1) {
            this.settings.loop = false;
            $('.slider-controls').css('display', 'none');
          }
        },
        items:1,
        loop: true,
        nav: false,
      });
    }
  }

}());



$(document).ready(function() {

  modal.init();
  headerScroll.init();
  banner.init();


  $('.lightgallery').lightGallery({
    selector: '.lg__photo',
    download: false,
    thumbnail: false
  });


  $('.slider__list').owlCarousel({
    items:5,
    center: true,
    nav: true,
    loop: true,
    navText:['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.4 20"  fill="#fff"><path d="M7.8 20h5.6L5.6 10l7.8-10H7.8L0 10"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 13.4 20" fill="#fff"><path d="M5.6 0H0l7.8 10L0 20h5.6l7.8-10"/></svg>'],
  });

  $('.photogallery').fotorama({
    allowfullscreen: true,
    nav: false,
    height: '439',
    width: '100%',
    loop: true
  });

  $('a[href^="tel:"]').attr('href', function(_,v){
      return v.replace(/\(0\)|\s+/g,'')
  });




  $('#callback-form').on('submit', function(e) {
    e.preventDefault();

    var $this = $(this);

    var data = {
        action : 'get_callback',
        nonce  : callbackHandler.nonce,
        title  : $this.find('input[name="title"]').val(),
        name   : $this.find('#name').val(),
        tel    : $this.find('#tel').val(),
        txt    : $this.find('#txt').val()
    };

    $.post(callbackHandler.url, data, function(response) {
        console.log(response);
        if (response.status == 'ok') {
          modal.success();
        }
    });
  });

/*
  var myMap;
  function init () {
      myMap = new ymaps.Map('map', {
          center:[60.710232, 28.749404], // Выборг
          zoom:10
      });
  }

  if($('#map').length) {
    // Дождёмся загрузки API и готовности DOM.
    ymaps.ready(init);
  }
*/
}());

$(window).scroll(function(){
  var left = $(this).scrollLeft();
  $('header .container').css('left',-left);
});
