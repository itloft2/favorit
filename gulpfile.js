var gulp = require('gulp'),
    // pug = require('gulp-pug'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),
    watch = require('gulp-watch');


// Pug
// gulp.task('pug', function(){
//   return gulp.src(['./assets/templates/*.pug', '!./assets/templates/*_.pug'])
//     .pipe(pug({ pretty: true }))
//     .pipe(gulp.dest('./build/'))
//     .pipe(browserSync.stream());
// });

gulp.task('sass', function(){
  return gulp.src('./assets/sass/*.scss')
    .pipe(sass({
      style: 'compressed',
      errLogToConsole: true,
      sourcemaps: false
    }))
    .on('error', sass.logError)
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: true
    }))
    // .pipe(cleanCSS())
    .pipe(gulp.dest('./wordpress/css/'))
    .pipe(browserSync.stream());
});

// JS
gulp.task('js', function(){
  return gulp.src('./assets/js/*.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./wordpress/js/'))
    .pipe(browserSync.stream());
});

// JS-vendor
gulp.task('vendorJS', function(){
  return gulp.src('./assets/js/vendor/*.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./wordpress/js/vendor/'))
    .pipe(browserSync.stream());
});

gulp.task('default', ['sass', 'js', 'vendorJS'], function() {
  browserSync.init({
      proxy: "favorit.loft/",
  });

  watch(['./assets/sass/**/*.scss', './assets/sass/**/_*.scss'], function(){
    gulp.start('sass');
  });
  watch('./assets/js/*.js', function(){
    gulp.start('js');
  });
  watch('./assets/js/vendor/*.js', function(){
    gulp.start('vendorJS');
  });
  watch('**/*.php', function(){
    browserSync.reload();
  });
});
